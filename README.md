vm_scripts
==========

Scripts to generate and manage images to be used in virtualization with the
goal to achieve some security by compartmentalization.
